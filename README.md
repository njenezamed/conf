# Setup a configuration of a developer computer

Run as root
```bash
./entrypoint.sh <username>
#or
wget -qO- https://gitlab.com/njenezamed/conf/-/raw/master/entrypoint.sh | bash -s -- <username>
```
```<username>``` is the username to create
